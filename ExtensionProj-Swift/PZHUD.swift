//
//  PZHUD.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/11/10.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit

class PZHUD: UIView {
    
    enum PZHUDType {
        case success
        case error
        case progress
        case text
        case record
        case unknown
    }
    
    // MARK: - Proprety
    fileprivate let hudLenght               =       156
    fileprivate let animationDuration       =       0.35
    fileprivate let hudCornerRadius         =       18
    
    fileprivate let defaultTintColor        =       UIColor.init(white: 0.15, alpha: 0.75)
    fileprivate let lightTintColor          =       UIColor.init(white: 0.95, alpha: 0.75)
    fileprivate let defaultContentColor     =       UIColor.init(white: 0.95, alpha: 1)
    fileprivate let lightContentColor       =       UIColor.init(white: 0.15, alpha: 1)
    
    fileprivate var title                   =       ""
    fileprivate var subTitle                =       ""
    
    var type: PZHUDType = .unknown
    
    fileprivate lazy var contentView: UIView = {
        let contentView: UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.hudLenght, height: self.hudLenght))
        contentView.center = self.center
        contentView.layer.cornerRadius = CGFloat(self.hudCornerRadius)
        contentView.layer.masksToBounds = true
        contentView.backgroundColor = self.defaultTintColor
        
        return contentView
    }()
    
    private lazy var hudView: UIView = {
        let hudView: UIView = UIView.init(frame: self.contentView.bounds)
        return hudView
    }()
    
    private lazy var titleLabel: UILabel = {
        let titleLabel: UILabel = UILabel.init()
        titleLabel.textAlignment = .center
        titleLabel.textColor = self.defaultContentColor
        titleLabel.font = UIFont.systemFont(ofSize: 17)
        titleLabel.numberOfLines = 0
        
        return titleLabel
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let subLabel: UILabel = UILabel.init()
        subLabel.textAlignment = .center
        subLabel.textColor = self.defaultContentColor
        subLabel.font = UIFont.systemFont(ofSize: 14)
        subLabel.numberOfLines = 0
        subLabel.adjustsFontSizeToFitWidth = true
        subLabel.minimumScaleFactor = 0.5
        
        return subLabel
    }()
    
    private var subLayer: CAShapeLayer = CAShapeLayer.init()
//    private var timer: DispatchSource
    
    
    // MARK: - Life Cycle
    func shareHUD() -> PZHUD {
        let hud = PZHUD.init()
        return hud
    }
    
    private init() {
        super.init(frame: UIScreen.main.bounds)
        addSubview(contentView)
        contentView.addSubview(hudView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subTitleLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let duration = 0.15
        
        if type == .text {
            subTitleLabel.frame = CGRect.zero
            let labelRect = calculateTextRect()
            titleLabel.frame = labelRect
            
            UIView.animate(withDuration: duration, animations: {
                self.contentView.frame = labelRect.insetBy(dx: -self.cornerRadius, dy: -self.cornerRadius)
                self.contentView.center = self.center
                self.hudView.frame = self.contentView.bounds
            })
            
        } else {
            titleLabel.frame = CGRect(x: 0, y: 0, width: hudLenght, height: 36)
            subTitleLabel.frame = CGRect(x: 0, y: hudLenght-30, width: hudLenght, height: 30)
            
            UIView.animate(withDuration: duration, animations: {
                self.contentView.frame = CGRect(x: 0, y: 0, width: self.hudLenght, height: self.hudLenght)
                self.contentView.center = self.center
                self.hudView.frame = self.contentView.bounds
                
            })
        }
        
    }
    
    // MARK: - Private
    private func set(title: String, subTitle: String) -> Void {
        self.title = title
        self.subTitle = subTitle
        titleLabel.text = title
        subTitleLabel.text = subTitle
    }
    
    private func calculateTextRect() -> CGRect {
        let textSize: CGSize = (title as NSString).boundingRect(with: CGSize.init(width: CGFloat(hudLenght), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSForegroundColorAttributeName: UIFont.systemFont(ofSize: 17)], context: nil).size
        let textHeight = UIScreen.main.bounds.size.height/2 < textSize.height ? UIScreen.main.bounds.size.height : textSize.height
        
        return CGRect(x: cornerRadius, y: cornerRadius, width: CGFloat(hudLenght), height: textHeight)
    }

    

}


















