//
//  PZTools.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/12/16.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit


class PZTools {
    
    class func safetyValue<T: Comparable>(value: T, lower: T, upper: T) -> T {
        guard lower <= upper else { return value }
        
        var newValue = value
        if value > upper {
            newValue = upper
        } else if value < lower {
            newValue = lower
        }
        
        return newValue
    }
    
    
    /// eg. 4000(s) -> 01:06:40 or 340(s) -> 05:40
    ///
    /// - Parameter seconds: 必须是遵循 Comparable 协议的对象
    /// - Returns: 类似于『01:06:40』格式的字符串
    class func formatterTime<T: Comparable>(seconds: T) -> String {
        var value: Int = 0
        if let a = seconds as? Int {
            value = a
        } else if let a = seconds as? UInt {
            value = Int(a)
        } else if let a = seconds as? Double {
            value = Int(a)
        } else if let a = seconds as? Float {
            value = Int(a)
        } else if let a = seconds as? String {
            value = (a as NSString).integerValue
        }
        
        guard value > 0 else { return "" }
        
        
        let h = value / 3600
        let remainder = value % 3600
        
        let m = remainder / 60
        let s = remainder % 60
        
        var str = ""
        if h > 0 {
            str = "\(String.init(format: "%.2d", h)):\(String.init(format: "%.2d", m)):\(String.init(format: "%.2d", s))"
        } else {
            str = "\(String.init(format: "%.2d", m)):\(String.init(format: "%.2d", s))"
        }
        
        return str
    }
    
    
}


// MARK: - Extension
extension Int {
    mutating func safetyValue(lower: Int, upper: Int) {
        self = PZTools.safetyValue(value: self, lower: lower, upper: upper)
    }
}

extension Float {
    mutating func safetyValue(lower: Float, upper: Float) {
        self = PZTools.safetyValue(value: self, lower: lower, upper: upper)
    }
}

extension Double {
    mutating func safetyValue(lower: Double, upper: Double) {
        self = PZTools.safetyValue(value: self, lower: lower, upper: upper)
    }
}

extension UInt {
    mutating func safetyValue(lower: UInt, upper: UInt) {
        self = PZTools.safetyValue(value: self, lower: lower, upper: upper)
    }
}

extension CGSize {
    func zoom(scale: CGFloat) -> CGSize {
        return CGSize(width: width * scale, height: height * scale)
    }
}

extension CGRect {
    func zoom(scale: CGFloat) -> CGRect {
        return CGRect(x: minX * scale, y: minY * scale, width: width * scale, height: height * scale)
    }
}















