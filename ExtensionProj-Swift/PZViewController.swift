//
//  PZViewController.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/11/8.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit

class PZViewController: UIViewController {
    
    // MARK: - Property
    
    /// 设置当前控制器所在tabBarController的item上角标内容
    var badgeValue: String? {
        didSet {
            guard let tabBarVC = tabBarController else { return }
            
            let index = tabBarVC.selectedIndex
            let item = tabBarVC.tabBar.items?[index]
            item?.badgeValue = badgeValue
        }
    }
    
    fileprivate var targetFrames : Array = [CGRect]()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButtonItem()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectSubviewsFrame()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Touch Action
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        let touchPoint = touches.first!.location(in: view)
        keyBoardResignWith(touchPoint: touchPoint)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        let touchPoint = touches.first!.location(in: view)
        keyBoardResignWith(touchPoint: touchPoint)
    }
    
    
    // MARK: - Setter
    private func setBackButtonItem() -> Void {
        let backItem = UIBarButtonItem.init()
        backItem.title = "";
        navigationItem.backBarButtonItem = backItem;
    }
    
    // MARK: - Private Methods
    
    /// 将当前控制器包含的视图中具有弹出键盘功能的视图的frame收集起来
    private func collectSubviewsFrame() -> Void {
        DispatchQueue.global().async {
            var frames = [CGRect]()
            for subView in self.view.subviews {
                if subView.conforms(to: UITextInput.self) {
                    frames.append(subView.frame)
                } 
            }
            
            DispatchQueue.main.async {
                self.targetFrames = frames
            }
            
        }
        
    }
    
    
    /// 根据传入的touchPoint判断当前点击位置是否在可编辑区域之外，如果是则收起键盘
    ///
    /// - Parameter touchPoint: 用户当前触摸位置
    private func keyBoardResignWith(touchPoint: CGPoint) -> Void {
        
        DispatchQueue.global().async {
            var isContain = false
            for rect in self.targetFrames {
                if rect.contains(touchPoint) == true {
                    isContain = true
                    break
                }
            }
            
            if !isContain {
                DispatchQueue.main.async {
                    UIApplication.shared.keyWindow?.endEditing(true)
                }
            }
        }
        
    }
    

}


// MARK: - Show Alert
extension PZViewController {
    typealias alertAction = ((UIAlertAction) -> Void)?
    
    func showAlert(title: String,
                   message: String,
                   sureButtonTitle: String,
                   sureHandler: alertAction) -> Void {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let sureAction = UIAlertAction.init(title: sureButtonTitle, style: .default, handler: { (action: UIAlertAction) in
            if sureHandler != nil {
                sureHandler!(action)
            }
        })
        
        alert.addAction(sureAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showAlert(title: String,
                   message: String,
                    sureButtonTitle: String,
                    cancelButtonTitle: String,
                    sureHandler: alertAction,
                    cancelHandler: alertAction) -> Void {
        
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let sureAction = UIAlertAction.init(title: sureButtonTitle, style: .default, handler:{ (action: UIAlertAction) in
            if sureHandler != nil {
                sureHandler!(action)
            }
        })
        
        let cancelAction = UIAlertAction.init(title: cancelButtonTitle, style: .cancel, handler: { (action: UIAlertAction) in
            if cancelHandler != nil {
                cancelHandler!(action)
            }
        })
        
        alert.addAction(sureAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}














