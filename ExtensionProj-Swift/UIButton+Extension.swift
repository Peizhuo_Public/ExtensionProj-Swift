//
//  UIButton+Extension.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2017/1/9.
//  Copyright © 2017年 peizhuo. All rights reserved.
//

import UIKit

public typealias ActionBlock = (_ sender: UIButton) -> Void

extension UIButton {
    // MARK: - Public
    public func clicked(click: @escaping ActionBlock) {
        actionBlock = click
        self.addTarget(self, action: #selector(buttonAction(sender:)), for: .touchUpInside)
        
    }
    
    
    public func setBackgroundColor(color: UIColor, for state: UIControlState) -> Void {
        let image = UIImage.imageWith(color: color, size: CGSize.zero)
        
        self.setBackgroundImage(image, for: state)
    }
    
    
    // MARK: - Ptivate
    fileprivate struct PropretyKeys {
        static var  actionBlockKey = "pz_actionBolck"
    }
    
    fileprivate var actionBlock: ActionBlock {
        get {
            return objc_getAssociatedObject(self, &PropretyKeys.actionBlockKey) as! ActionBlock
        }
        set {
            objc_setAssociatedObject(self, &PropretyKeys.actionBlockKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    
    @objc fileprivate func buttonAction(sender: UIButton) -> Void {
        actionBlock(sender)
    }
    
    
}




//extension UIControl {
//    
//    private struct VarKeys {
//        static var allowRippleKey           = "pz_allowRipple"
//    }
//    
//    
//    public var allowRipple: Bool {
//        get {
//            return objc_getAssociatedObject(self, &VarKeys.allowRippleKey) as? Bool ?? false
//        }
//        set {
//            objc_setAssociatedObject(self, &VarKeys.allowRippleKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
//        }
//    }
//    
//    
//    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        super.touchesEnded(touches, with: event)
//        
//        if allowRipple == true {
//            let touchPoint = touches.first!.location(in: self)
//            let rippleView = getRippleView(touchPoint: touchPoint)
//            addSubview(rippleView)
//            
//            beginRippleAniamtion(rippleView: rippleView)
//            
//        }
//        
//    }
//    
//    
//    
//    
//}















