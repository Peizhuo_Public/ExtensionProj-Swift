//
//  UIImage+Extension.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2017/1/10.
//  Copyright © 2017年 peizhuo. All rights reserved.
//

import UIKit


extension UIImage {
    
    /// 类方法 - 根据颜色和尺寸绘制并返回对应的图片
    ///
    /// - Parameters:
    ///   - color: 图片颜色
    ///   - size: 图片尺寸
    /// - Returns:
    class public func imageWith(color: UIColor, size: CGSize) -> UIImage {
        var imgSize = size
        if imgSize.equalTo(CGSize.zero) {
            imgSize = CGSize(width: 1, height: 1)
        }
        
        var image = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(imgSize, true, UIScreen.main.scale)
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(origin: CGPoint.zero, size: imgSize))
            image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        }
        
        UIGraphicsEndImageContext()
        return image
    }
    
    
    /// 根据缩放倍数缩放并返回缩放后的新图片
    ///
    /// - Parameter scale: 缩放倍数
    /// - Returns: 缩放后的图片：缩放失败会返回原始图片
    public func zoom(scale: CGFloat) -> UIImage {
        let newSize = CGSize(width: self.size.width * scale, height: self.size.height * scale)
        var newImage = self
        
        newImage = resize(newSize: newSize);
        
        return newImage
    }
    
    
    /// 根据传入的尺寸重新绘制并返回新图片：只重绘不做拉伸和适应处理，如果传入尺寸的宽高比与原始图片不一致，可能导致图片比例失真
    ///
    /// - Parameter newSize: 重绘尺寸
    /// - Returns: 重绘后的新图片：重绘失败会返回原始图片
    public func resize(newSize: CGSize) -> UIImage {
        var newImage = self
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, UIScreen.main.scale)
        draw(in: CGRect(origin: CGPoint.zero, size: newSize))
        newImage = UIGraphicsGetImageFromCurrentImageContext() ?? self
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    /// 裁剪图片
    ///
    /// - Parameters:
    ///   - rect: 裁剪范围
    ///   - rotationAngle: 旋转角度：该参数目前未启用，传任意值均可
    /// - Returns: 裁剪后的图片：裁剪失败会返回原始图片
    public func cropWith(rect: CGRect, rotationAngle: Double) -> UIImage {
        var cropImage = self
        
        UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
        if let cgImg = cgImage?.cropping(to: rect) {
            cropImage = UIImage.init(cgImage: cgImg, scale: self.scale, orientation: self.imageOrientation)
        }
        UIGraphicsEndImageContext()
        
        return cropImage
    }
    
    
    /// 全屏截图
    class public func fullScreenshot() -> UIImage {
        return imageWith(view: UIApplication.shared.keyWindow!)
    }
    
    
    /// 截取指定视图
    class public func imageWith(view: UIView) -> UIImage {
        return imageWith(view: view, rect: view.bounds)
    }
    
    
    /// 截取指定视图的指定范围
    class public func imageWith(view: UIView, rect: CGRect) -> UIImage {
        var image = UIImage()
        let scale = UIScreen.main.scale
        let size = view.frame.size
        
        UIGraphicsBeginImageContextWithOptions(size, true, scale)
        if let context = UIGraphicsGetCurrentContext() {
            context.saveGState()
            UIRectClip(rect)
            view.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()!
        }
        UIGraphicsEndImageContext()
        
        return image
    }
    
    
}


extension UIColor {
    
    /// 获取随机色
    class public func randomColor() -> UIColor {
        let upper: UInt32 = 255
        return UIColor(colorLiteralRed: Float(randomNum(upper: upper))/255.0, green: Float(randomNum(upper: upper))/255.0, blue: Float(randomNum(upper: upper))/255.0, alpha: 1)
    }
    
    
    class fileprivate func randomNum(upper: UInt32) -> UInt32 {
        return arc4random_uniform(upper)
    }
    
    
}









