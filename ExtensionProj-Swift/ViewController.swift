//
//  ViewController.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/11/8.
//  Copyright © 2016 peizhuo. All rights reserved.
//

import UIKit

class ViewController: PZViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var rippleView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        automaticallyAdjustsScrollViewInsets = false
        
//        imageView.allowSpring = true
        if #available(iOS 9.0, *) {
            imageView.allowForceTouchScale = true
        } else {
            // Fallback on earlier versions
        }
//        imageView.maxForceTouchScale = 1.2
        
//        if "1880018" + "0063" =~ "^1\\d{10}" {
//            print_debug("是手机号")
//        }
        
//        let isEmail = "123@163.com".isEmail()
//        PZLog(isEmail)
        
        
//        let button = UIButton.init(type: .system)
//        button.frame = CGRect(x: 20, y: 84, width: 150, height: 60)
//        button.backgroundColor = UIColor.flatOrange()
//        
//        view.addSubview(button)
        
        
//        let propertyList = button.getPropertyNameList()
//        print(propertyList)
        
        
        
//        let sel = #selector(testMethod(str:))
//        let success = button.addMethod(sel: sel)
//        print(success)
//
//        if button.responds(to: sel) {
//            button.perform(sel, with: "testMethod")
//        }
        
//        self.methodSwizzling(originSEL: #selector(viewWillAppear(_:)), swizzledSEL: #selector(pz_viewWillAppear(_:)))
        
        
        let img = UIImage.imageWith(color: UIColor.flatGreen(), size: CGSize(width: 200, height: 200))
        
        let newImage = img.zoom(scale: 0.25)
        
        print_debug(img.size)
        print_debug(newImage.size)
        
        
//        let image = #imageLiteral(resourceName: "image.png")
//        imageView.image = image
//        
//        let cropImg = image.cropWith(rect: CGRect.init(x: 0, y: 0, width: 100, height: 100), rotationAngle: 0)
//        
//        button.clicked { (sender) in
//            self.imageView.image = cropImg
//        }
        
        
//        imageView.layer.borderWidth = 1
//        imageView.layer.borderColor = #colorLiteral(red: 0.1686089337, green: 0.1686392725, blue: 0.1686022878, alpha: 1).cgColor
//        button.clicked { (sender) in
////            let img = UIImage.fullScreenshot()
//            let img = UIImage.imageWith(view: button)
//            self.imageView.image = img
//        }
        
        let isMusic = false
//        http://s1.music.126.net/download/osx/NeteaseMusic_1.5.1_530_web.dmg
        let url = URL.init(string: isMusic == true ? "http://s1.music.126.net/download/osx/NeteaseMusic_1.5.1_530_web.dmg" : "http://dldir1.qq.com/qqfile/QQforMac/QQ_V5.4.1.dmg")!
        let down = Downloader.init(downloadURL: url, writeFilePath: nil)
        down.delegate = self
        progressView.progress = Float(down.progress)
        
//        let down1 = Downloader.init(downloadURL: URL.init(string: "http://s1.music.126.net/download/osx/NeteaseMusic_1.5.1_530_web.dmg")!, writeFilePath: nil)
        
        
//        button.clicked { (sender) in
//            let color = UIColor.randomColor()
//            sender.backgroundColor = color
//            print_debug(color)
//        }
        
        
        print_debug(NSHomeDirectory())
        downloadButton.clicked { (sender) in
            down.start()
//            down1.start()
        }
        
        pauseButton.clicked { (sender) in
            down.pause()
        }
        
        
        
        rippleView.allowRipple = true
        
        
    }
    
    
    dynamic func pz_viewWillAppear(_ animated: Bool) {
        pz_viewWillAppear(animated)
        
        print_debug("😏")
    }
    
    
    @IBAction func rippleControlAction(_ sender: UIButton) {
        
        print_debug("😏")
    }
    

    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        let touchPoint = touches.first!.location(in: view)
        if imageView.frame.contains(touchPoint) == true {
            performSegue(withIdentifier: "Push", sender: nil) 
//            performSegue(withIdentifier: "PresentMediaVC", sender: nil)

        }
    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentMediaVC" {
            if let playerVC = (segue.destination as? AVPlayerVC) {
                let filePath = Bundle.main.path(forResource: "HoloLens_2", ofType: "mp4")
                let url = URL.init(fileURLWithPath: filePath!)
                
                playerVC.url = url
            }
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


extension ViewController: DownloaderDelegate {
    
    func downloader(progress: Double, url: URL?) {
        print_debug(progress)
        progressView.progress = Float(progress)
    }
    
    
    func downloader(didFinished: Bool, url: URL?, filePath: String) {
        print_debug("下载完成")
        print_debug(url!)
        print_debug(filePath)
    }
    
    
    
}
















