//
//  AVPlayerVC.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/12/12.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer
import SnapKit
import ChameleonFramework


class AVPlayerVC: AVPlayerViewController {
    // MARK: - let & var
    var url: URL? = nil {
        didSet {
            if let u = url {
                configPlayer(url: u)
            }
        }
    }
    
    fileprivate lazy var volumeView: MPVolumeView = {
        let volumeView = MPVolumeView.init(frame: CGRect(x: -1000, y: -1000, width: 10, height: 10))
        volumeView.showsVolumeSlider = true
        volumeView.showsRouteButton = true
        volumeView.sizeToFit()
        
        for sView in volumeView.subviews {
            if sView.isKind(of: UISlider.self) {
                self.volumeSlider = (sView as! UISlider)
                break
            }
        }
        
        return volumeView
    }()
    fileprivate var volumeSlider: UISlider?
    fileprivate var doubleTap: UITapGestureRecognizer?
    fileprivate var playerView: UIView?
    fileprivate var timeLabel: UILabel?


    fileprivate lazy var lightRect: CGRect = {
        return CGRect(x: 0, y: 0, width: self.view.width/3, height: self.view.height)
    }()

    fileprivate lazy var volumeRect: CGRect = {
        return CGRect(x: self.view.width/3*2, y: 0, width: self.view.width, height: self.view.height)
    }()

    fileprivate var swipeOrigin: CGPoint = CGPoint.zero
    fileprivate var isSkip: Bool = false
    fileprivate var sliderValue: Float = 0.0
    fileprivate var brightness: CGFloat = 0.0
    fileprivate var currentTime: CMTime = CMTime()
    
    
    // MARK: - Lift Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addGesture()
        initTimeLabel()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIScreen.main.brightness = brightness
    }
    
    
    // MARK: - Config Subviews
    func configPlayer(url: URL) -> Void {
        player = AVPlayer.init(url: url)
        player?.play()
        
        view.addSubview(volumeView)
    }
    
    func addGesture() -> Void {
        playerView = view.subviews.first
        guard let aPlayerView = playerView else { return }
        
        // 以自定义双击手势替换掉系统默认的双击手势
        let newDoubleTap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(doubleTapAction(sender:)))
        newDoubleTap.numberOfTapsRequired = 2
        
        for gesture in aPlayerView.gestureRecognizers! {
            
            if let tap = gesture as? UITapGestureRecognizer {
                if tap.numberOfTapsRequired == 2 {               // 单指双击
                    aPlayerView.removeGestureRecognizer(tap)
                    aPlayerView.addGestureRecognizer(newDoubleTap)
                } else if tap.numberOfTapsRequired == 1 {        // 单指单击
                    tap.require(toFail: newDoubleTap)
                }
            }

        }

    }
    
    
    func initTimeLabel() -> Void {
        let label = UILabel.init()
        label.textAlignment = .center
        label.layer.cornerRadius = 5
        label.layer.masksToBounds = true
        label.alpha = 0
        label.backgroundColor = UIColor.flatBlack()
        label.textColor = UIColor.flatWhite()
        label.text = "00:00/00:00"
        
        view.addSubview(label)
        timeLabel = label
        
        label.snp.makeConstraints { (make) in
            make.width.greaterThanOrEqualTo(110)
            make.height.equalTo(45)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(9/16.0)
        }
        
    }
    
    
    // MARK: - Action Methods
    func doubleTapAction(sender: UITapGestureRecognizer) -> Void {
        player?.rate == 0 ? player?.play() : player?.pause()
    }
    
    func setTimeLabelText(currentTime: CMTime, duration: CMTime?) -> Void {
        guard let dur = duration else { return }
        
        let durationSeconds = Float(dur.value) / Float(dur.timescale)
        var currentSeconds = Float(currentTime.value) / Float(currentTime.timescale)
        
        currentSeconds.safetyValue(lower: 0, upper: durationSeconds)
        
        let durationText = PZTools.formatterTime(seconds: durationSeconds)
        let currentText = PZTools.formatterTime(seconds: currentSeconds)
        
        timeLabel?.text = "\(currentText)/\(durationText)"
    }
    
    func showTimeLabel(isShow: Bool) -> Void {
        UIView.animate(withDuration: 0.2) { 
            self.timeLabel?.alpha = isShow == true ? 0.6 : 0
        }
    }
    
    
}


// MARK: - Touch Action
extension AVPlayerVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        guard touches.count == 1 else { return }
        let touchPoint = touches.first!.location(in: view)
        
        swipeOrigin = touchPoint
        sliderValue = self.volumeSlider?.value ?? 0
        brightness = UIScreen.main.brightness
        currentTime = player?.currentTime() ?? CMTime()
        
        view.bringSubview(toFront: timeLabel!)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        guard touches.count == 1 else { return }
        let touchPoint = touches.first!.location(in: view)
        
        let value_x = swipeOrigin.x - touchPoint.x
        var value_y = swipeOrigin.y - touchPoint.y
        
        if fabsf(Float(value_x)) > fabsf(Float(value_y)) {    // 左右滑动 - 调整进度
            guard let aPlayer = player else { return }
            let second = -value_x * 180/view.width
            
            let time = CMTime(seconds: Double(currentTime.value)/Double(currentTime.timescale) + Double(second/2), preferredTimescale: currentTime.timescale)
            
            setTimeLabelText(currentTime: time, duration: aPlayer.currentItem?.duration)
            showTimeLabel(isShow: true)
 
            
            isSkip = true
        } else {                                              // 上下滑动
            value_y /= view.height

            if self.lightRect.contains(touchPoint) {              // 调整亮度
                
                let light = value_y + brightness
                UIScreen.main.brightness = light > 1 ? 1 : (light < 0 ? 0 : light)
                
            } else if self.volumeRect.contains(touchPoint) {      // 调整音量
                
                let volume: Float = Float(value_y) + sliderValue
                volumeSlider?.value = volume > 1 ? 1 : (volume < 0 ? 0 : volume)
                
            }
            
            isSkip = false
        }
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        guard touches.count == 1 else { return }
        let touchPoint = touches.first!.location(in: view)

        sliderValue = volumeSlider?.value ?? 0
        
        if isSkip == true {
            let second = (touchPoint.x - swipeOrigin.x) * 180/view.width
            let time = CMTime(seconds: Double(currentTime.value)/Double(currentTime.timescale) + Double(second/2), preferredTimescale: currentTime.timescale)
            
            player?.seek(to: time)
            isSkip = false
        }
        
        
        currentTime = player?.currentTime() ?? CMTime()
        perform(#selector(showTimeLabel(isShow:)), with: false, afterDelay: 1)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
    }
    
}





//// MARK: - AVPlayerViewControllerDelegate
//@available(iOS 8.0, *)
//extension AVPlayerVC : AVPlayerViewControllerDelegate {
//    func playerViewControllerWillStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
//        
//    }
//    
//    
//    func playerViewControllerDidStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
//        
//        
//    }
//    
//    
//    func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
//        
//    }
//    
//    
//    func playerViewControllerWillStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
//        
//    }
//    
//    
//    func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
//        
//    }
//    
//    
//    func playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart(_ playerViewController: AVPlayerViewController) -> Bool {
//        
//        return true
//    }
//    
//    
//    func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
//        
//    }
//    
//}
























