//
//  UIView+Extension.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/11/8.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit


// MARK: - Position
extension UIView {
    
    public var height: CGFloat {
        get { return frame.size.height }
        set { frame.size.height = newValue }
    }
    
    public var width: CGFloat {
        get { return frame.size.width }
        set { frame.size.width = newValue }
    }
    
    public var top: CGFloat {
        get { return frame.origin.y }
        set { frame.origin.y = newValue }
    }
    
    public var left: CGFloat {
        get { return frame.origin.x }
        set { frame.origin.x = newValue }
    }
    
    public var bottom: CGFloat {
        get { return frame.origin.y + frame.size.height }
        set { frame.origin.y = newValue - frame.size.height }
    }
    
    public var right: CGFloat {
        get { return frame.origin.x + frame.size.width }
        set { frame.origin.x = newValue - frame.size.width }
    }
    
    public var size: CGSize {
        get { return frame.size }
        set { frame.size = newValue }
    }
    
    public var origin: CGPoint {
        get { return frame.origin }
        set { frame.origin = newValue }
    }
    
    public var centerX: CGFloat {
        get { return center.x }
        set { center.x = newValue }
    }
    
    public var centerY: CGFloat {
        get { return center.y }
        set { center.y = newValue }
    }
    
    public var topRight: CGPoint {
        get { return CGPoint(x: right, y: top) }
    }
    
    public var bottomLeft: CGPoint {
        get { return CGPoint(x: left, y: bottom) }
    }
    
    public var bottomRight: CGPoint {
        get { return CGPoint(x: right, y: bottom) }
    }
    
//    /**
//     计算传入的Rect的中心点坐标
//     */
//    func getCenterWith(_ rect: CGRect) -> CGPoint {
//        return CGPoint(x: rect.midX, y: rect.midY)
//    }
//    
//    /// 相对起点坐标缩放
//    func scaleRelativeOriginBy(_ scaleFactor: CGFloat) {
//        var newFrame = self.frame
//        newFrame.size.width  *= scaleFactor
//        newFrame.size.height *= scaleFactor
//        frame = newFrame
//    }
//    
//    /// 相对中心点坐标缩放
//    func scaleRelativeCenterBy(_ scaleFactor: CGFloat) {
//        var newFrame = self.frame
//        let oldCenter = center
//        newFrame.size.width  *= scaleFactor
//        newFrame.size.height *= scaleFactor
//        self.frame = newFrame
//        self.center = oldCenter
//    }
    
}


// MARK: - CGRect Extension
extension CGRect {
    /// 获取中心点坐标
    func center() -> CGPoint {
        return CGPoint(x: self.midX, y: self.midY)
    }
}


// MARK: - Response Chain
extension UIView {
    /// 通过响应者链寻找视图控制器
    ///
    /// - Returns: 当前视图所在的UIViewController，如果当前视图不在控制器中， 则返回nil
    func viewController() -> UIViewController? {
        var nxt = next
        
        repeat {
            if nxt?.isKind(of: UIViewController.self) == true {
                return nxt as? UIViewController
            }
            nxt = next?.next
        }while (nxt != nil)
        
        return nil
    }
    
    /// 通过响应者链寻找导航控制器
    ///
    /// - Returns: 当前视图所在的UINavigationController，如果当前视图不在导航控制器中， 则返回nil
    func navigationController() -> UINavigationController? {
        return viewController()?.navigationController
    }
    
    /// 自动根据所属类从同类名XIB中获取并返回当前类对象
    /// 默认情况下两个参数均可传nil
    /// 如果一个XIB文件中包含多个视图，则默认返回第一个
    func viewFromNibByDefaultClassName(owner: Any?, option: Dictionary<String, Any>?) -> Any? {
        return Bundle.main.loadNibNamed(NSStringFromClass(object_getClass(self)), owner: owner, options: option)?.first
    }
    
}


// MARK: - IBInspectable
@IBDesignable
extension UIView {
    
    /// 可在storyboard/XIB中直接设置圆角
    @IBInspectable public var cornerRadius: CGFloat {
        get{ return layer.cornerRadius }
        set{
            layer.cornerRadius = newValue
            layer.shouldRasterize = true
            layer.masksToBounds = (newValue > 0)
        }
    }
    
    /// 可在storyboard/XIB中直接设置边线宽度
    @IBInspectable public var borderWidth: CGFloat {
        get{ return layer.borderWidth }
        set{ layer.borderWidth = newValue }
    }
    
    /// 可在storyboard/XIB中直接设置边线颜色
    @IBInspectable public var borderColor: UIColor? {
        get {
            return (layer.borderColor == nil ? UIColor.init(cgColor: layer.borderColor!) : nil)
        }
        set { layer.borderColor = newValue?.cgColor }
        
    }
    
}

let animationDuration = 0.1

// MARK: - Animation

extension UIView {
    
    private struct PropretyKeys {
        static var allowSpringKey           = "pz_allowSpring"
        static var allowForceTouchScaleKey  = "pz_allowForceTouchScale"
        static var maxForceTouchScaleKey    = "pz_maxForceTouchScale"
        static var allowRippleKey           = "pz_allowRipple"
    }
    
    /// 设置为true时，当前视图将具备按下缩小，松手回弹的动画效果（会自动将isUserInteractionEnabled设置为true，无需重复设置），默认为false
    public var allowSpring : Bool {
        get {
            return objc_getAssociatedObject(self, &PropretyKeys.allowSpringKey) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &PropretyKeys.allowSpringKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue == true { self.isUserInteractionEnabled = newValue }
        }
    }
    
    
    /// 设置为true时，当前视图会跟随按压屏幕的力度缩放（需要ForceTouch屏幕支持，会自动将isUserInteractionEnabled设置为true，无需重复设置），默认为false
    @available(iOS 9.0, *) public var allowForceTouchScale : Bool {
        get {
            return objc_getAssociatedObject(self, &PropretyKeys.allowForceTouchScaleKey) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &PropretyKeys.allowForceTouchScaleKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            if newValue == true { self.isUserInteractionEnabled = newValue }
        }
    }
    
    /// 视图根据按压力度缩放的最大倍数，默认为1.2，需与allowForceTouchScale属性配合使用，单独设置无效
    @available(iOS 9.0, *) public var maxForceTouchScale: Float {
        get {
            return objc_getAssociatedObject(self, &PropretyKeys.maxForceTouchScaleKey) as? Float ?? 1.2
        }
        set {
            if newValue > 1.0 {
                objc_setAssociatedObject(self, &PropretyKeys.maxForceTouchScaleKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
            }
        }
    }
    
    public var allowRipple: Bool {
        get {
            return objc_getAssociatedObject(self, &PropretyKeys.allowRippleKey) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &PropretyKeys.allowRippleKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if allowSpring == true {
            let scale: CGFloat = 0.97
            UIView.animate(withDuration: animationDuration, animations: {
                self.transform = CGAffineTransform.init(scaleX: scale, y: scale)
            })
            return
        }
        
    }
    
    open override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        if #available(iOS 9.0, *) {
            if allowForceTouchScale == true {
                let touch = touches.first!
                let scale = touch.force / touch.maximumPossibleForce * CGFloat(maxForceTouchScale - 1) + 1
                UIView.animate(withDuration: animationDuration, animations: {
                    self.transform = CGAffineTransform.init(scaleX: scale, y: scale)
                })
            }
        }
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if allowSpring == true {
            let scale: CGFloat = 1.03
            UIView.animate(withDuration: animationDuration, animations: {
                self.transform = CGAffineTransform.init(scaleX: scale, y: scale)
            }, completion: { (conpletion : Bool) in
                UIView.animate(withDuration: animationDuration, animations: {
                    self.transform = CGAffineTransform.identity
                })
            })
            return
        }
        

        if #available(iOS 9.0, *) {
            if allowForceTouchScale == true {
                UIView.animate(withDuration: animationDuration, animations: { 
                    self.transform = CGAffineTransform.identity
                })
            }
        }
        
        
        if allowRipple == true {
            let touchPoint = touches.first!.location(in: self)
            let rippleView = getRippleView(touchPoint: touchPoint)
            addSubview(rippleView)
            
            beginRippleAniamtion(rippleView: rippleView)
            
        }
        
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if #available(iOS 9.0, *) {
            if allowForceTouchScale == true {
                UIView.animate(withDuration: animationDuration, animations: {
                    self.transform = CGAffineTransform.identity
                })
            }
        }
        
    }
    
    
    
}


// MARK: - Private Methods
extension UIView {
    
    func getRippleView(touchPoint: CGPoint) -> UIView {
        let diagonal = getDiagonal(touchPoint: touchPoint)
        let scale: CGFloat = 0.01
        
        let rippleView = UIView.init(frame: CGRect(x: 0, y: 0, width: diagonal, height: diagonal))
        rippleView.center = touchPoint
        rippleView.backgroundColor = UIColor.init(white: 0.9, alpha: 0.5)
        rippleView.layer.cornerRadius = diagonal/2.0
        rippleView.layer.masksToBounds = true
        layer.masksToBounds = true
        
        rippleView.transform = CGAffineTransform.init(scaleX: scale, y: scale)
        
        return rippleView
    }
    
    fileprivate func getDiagonal(touchPoint: CGPoint) -> CGFloat {
        
        var point = CGPoint.zero
        if touchPoint.x < bounds.center().x && touchPoint.y < bounds.center().y {
            point = CGPoint(x: width, y: height)
        } else if touchPoint.x > bounds.center().x && touchPoint.y < bounds.center().y {
            point = CGPoint(x: 0, y: height)
        } else if touchPoint.x < bounds.center().x && touchPoint.y > bounds.center().y {
            point = CGPoint(x: width, y: 0)
        }
        
        let diagonal = sqrtf(Float(pow(abs(touchPoint.x - point.x), 2) + pow((touchPoint.y - point.y), 2))) * 2
        
        return CGFloat(diagonal)
    }
    
    func beginRippleAniamtion(rippleView: UIView) -> Void {
        let duration = getAnimationDuration()
        
        UIView.animate(withDuration: TimeInterval(duration), animations: {
            
            rippleView.transform = CGAffineTransform.identity
            
        }, completion: { (finished) in
            
            UIView.animate(withDuration: 0.15, animations: {
                rippleView.alpha = 0
            }, completion: { (fini) in
                rippleView.removeFromSuperview()
            })
            
        })
    }
    
    
    fileprivate func getAnimationDuration() -> CGFloat {
        let unitNum: CGFloat  = 12.0     // 单位长度 0.01s/12px
        let duration: CGFloat = max(width, height) / unitNum * 0.01
        
        return duration < 0.25 ? 0.25 : duration
    }
    
    
}













