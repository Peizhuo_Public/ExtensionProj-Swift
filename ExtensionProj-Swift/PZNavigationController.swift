//
//  PZNavigationController.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/11/9.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit

class PZNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        replacePopGesture()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func replacePopGesture() {
        let sel: Selector = Selector(("handleNavigationTransition:"))
        let target = interactivePopGestureRecognizer?.delegate
        
        if target?.responds(to: sel) == true {
            let pan = UIPanGestureRecognizer.init(target: target, action: sel)
            pan.delegate = self
            view.addGestureRecognizer(pan)
        }
    }

}


// MARK: - UIGestureRecognizerDelegate
extension PZNavigationController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return viewControllers.count != 1
    }
}







