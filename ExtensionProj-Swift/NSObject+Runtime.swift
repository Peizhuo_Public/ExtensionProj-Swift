//
//  NSObject+Runtime.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2017/1/9.
//  Copyright © 2017年 peizhuo. All rights reserved.
//

import Foundation

extension NSObject {
    
    /// 通过runtime获取当前类所有属性名
    ///
    /// - Returns: 属性名称数组
    public func getPropertyNameList() -> [String] {
        
        var outCount: UInt32 = 0
        let properties: UnsafeMutablePointer<objc_property_t?>! = class_copyPropertyList(self.classForCoder, &outCount)
        
        var propertyList = [String]()
        for i in 0...Int(outCount)-1 {
            let property = properties[i]
            let proName = String(utf8String: property_getName(property)) ?? ""
            
            propertyList.append(proName)
        }
        
        return propertyList
    }
    
    
    /// 通过runtime获取当前类所有方法
    ///
    /// - Returns: 方法数组
    public func getMethodList() -> [Selector?] {
        var outCount: UInt32 = 0
        let methods: UnsafeMutablePointer<Method?>! = class_copyMethodList(self.classForCoder, &outCount)
        
        var methodList = [Selector?]()
        for i in 0...Int(outCount)-1 {
            let method = methods[i]
            let sel = method_getName(method)
            
            methodList.append(sel)
        }
        
        return methodList
    }
    
    
    /// 通过runtime当前类所有方法名
    ///
    /// - Returns: 方法名数组
    public func getMethodNameList() -> [String] {
        let methodList = getMethodList()
        var methodNameList = [String]()
        
        for sel in methodList {
            let methodName = String(utf8String: sel_getName(sel)) ?? ""
            
            methodNameList.append(methodName)
        }
        
        return methodNameList
    }
    
    
    /// 根据传入的属性名获取该属性
    ///
    /// - Parameter name: 属性名
    /// - Returns: Ivar
    public func ivarForName(name: String) -> Ivar! {
        return class_getClassVariable(self.classForCoder, name)
    }
    
    
    /// 根据传入的属性名获取该属性的值
    ///
    /// - Parameter name: 属性名
    /// - Returns: 属性值
    public func valueForName(name: String) -> Any? {
        let ivar = ivarForName(name: name)
        let value = object_getIvar(self, ivar!) ?? nil
        
        return value
    }
    
    
    /// 根据传入的属性名和属性值为该属性重新赋值
    ///
    /// - Parameters:
    ///   - name: 属性名
    ///   - value: 属性值
    public func setIvarValue(name: String, value: Any!) -> Void {
        let ivar = ivarForName(name: name)
        object_setIvar(self, ivar, value)
    }
    
    
    
    /// 根据传入的属性名获取该属性类型
    ///
    /// - Parameter name: 属性名
    /// - Returns: 属性类型
    public func getIvarType(name: String) -> AnyClass? {
        if let typeName = getIvarTypeName(name: name) {
            return NSClassFromString(typeName)
        }
        
        return nil
    }
    
    /// 根据传入的属性名获取该属性类型名
    ///
    /// - Parameter name: 属性名
    /// - Returns: 属性类型名
    public func getIvarTypeName(name: String) -> String! {
        let ivar = ivarForName(name: name)
        let typeEncoding: UnsafePointer<Int8>! = ivar_getTypeEncoding(ivar)
        
        let typeName = String(utf8String: typeEncoding) ?? ""
        
        return typeName
    }
    
    
    /// 在运行时为类增加方法
    ///
    /// - Parameter sel: 需要增加的方法
    /// - Returns: 添加是否成功
    public func addMethod(sel: Selector) -> Bool {
        let method = class_getClassMethod(self.classForCoder, sel)
        let imp = method_getImplementation(method)
        let type = method_getTypeEncoding(method)
        
        class_addMethod(self.classForCoder, sel, imp, type)
        
        return self.responds(to: sel)
    }
    
    
    
    /// 方法交换：由于Swift语言不具备OC的runtime机制，请确保交换的方法前使用“dynamic”关键字修饰，否则将无法实现预期效果
    ///
    /// - Parameters:
    ///   - originSEL: 需要被交换的初始方法
    ///   - swizzledSEL: 交换方法
    public func methodSwizzling(originSEL: Selector!, swizzledSEL: Selector!) -> Void {
        let originMethod = class_getInstanceMethod(self.classForCoder, originSEL)
        let swizzledMethod = class_getInstanceMethod(self.classForCoder, swizzledSEL)
        
        let didAddMethod = class_addMethod(self.classForCoder, originSEL, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        
        if didAddMethod {
            class_replaceMethod(self.classForCoder, swizzledSEL, method_getImplementation(originMethod), method_getTypeEncoding(originMethod))
        } else {
            method_exchangeImplementations(originMethod, swizzledMethod)
        }
        
    }
    
    /// 方法交换：由于Swift语言不具备OC的runtime机制，请确保交换的方法前使用“dynamic”关键字修饰，否则将无法实现预期效果
    ///
    /// - Parameters:
    ///   - originSEL: 需要被交换的初始方法
    ///   - swizzledSEL: 交换方法
    class public func methodSwizzling(originSEL: Selector!, swizzledSEL: Selector!) -> Void {
        let originMethod = class_getInstanceMethod(self, originSEL)
        let swizzledMethod = class_getInstanceMethod(self, swizzledSEL)
        
        let didAddMethod = class_addMethod(self, originSEL, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
        
        if didAddMethod {
            class_replaceMethod(self, swizzledSEL, method_getImplementation(originMethod), method_getTypeEncoding(originMethod))
        } else {
            method_exchangeImplementations(originMethod, swizzledMethod)
        }
        
    }

    
    
}

















