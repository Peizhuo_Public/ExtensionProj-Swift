//
//  Downloader.swift
//  ExtensionProj-Swift
//
//  Created by 李沛倬 on 2017/2/20.
//  Copyright © 2017年 peizhuo. All rights reserved.
//

import UIKit

@objc protocol DownloaderDelegate: NSObjectProtocol {
    @objc optional func downloader(progress: Double, url: URL?) -> Void
    @objc optional func downloader(didFinished: Bool, url: URL?, filePath: String) -> Void
}

let DOWNLOAD_KEY_RECEIVELENGTH     = "ReceiveLength"
let DOWNLOAD_KEY_TOTALLENGTH       = "TotalLength"
let DOWNLOAD_KEY_FILENAME          = "filename"
let DOWNLOADER_INFODIC             = "DOWNLOADER_INFODIC"
let DOWNLOAD_MAXCACHE              = 500 * 1000         // ~=500kb


class Downloader: NSObject {
    // MARK: - var
    public var progress: Double = 0.0
    public var delegate: DownloaderDelegate?
    
    
    fileprivate var downloadURL: URL
    fileprivate var writePath: String
    
    fileprivate var connection: NSURLConnection = NSURLConnection.init()
    fileprivate var isDownload: Bool        = false
    
    fileprivate var totalLength: Int     = 0
    fileprivate var receiveLength: Int   = 0 {
        didSet {
            if totalLength > 0 {
                progress = Double(receiveLength) / Double(totalLength)
            }
        }
    }
    
    fileprivate var receiveData: Data = Data.init()

    
    // MARK: - init
    
    /// 初始化方法
    ///
    /// - Parameters:
    ///   - downloadURL: 下载链接
    ///   - writeFilePath: 文件存放路径（为空时将默认存放在/Library/目录下，以downloadURL.lastPathComponent为文件名）
    init(downloadURL: URL, writeFilePath: String?) {
        self.downloadURL = downloadURL
        self.writePath = writeFilePath ?? NSHomeDirectory().appending("/Library/\(downloadURL.lastPathComponent)")
        
        super.init()
        
        let infoDic = getInfoDic()
        totalLength     = infoDic[DOWNLOAD_KEY_TOTALLENGTH] as? Int ?? 0
        receiveLength   = infoDic[DOWNLOAD_KEY_RECEIVELENGTH] as? Int ?? 0
        progress        = totalLength > 0 ? Double(receiveLength) / Double(totalLength) : 0
    }
    
    
    
    // MARK: - Public Methods
    
    func start() -> Void {
        guard isDownload == false else { return }
        
        var request = URLRequest.init(url: downloadURL)
        if receiveLength > 0 {
            request.setValue("bytes=\(receiveLength)-", forHTTPHeaderField: "Range")
        }
        
        connection = NSURLConnection.init(request: request, delegate: self)!
        isDownload = true
        
        if FileManager.default.fileExists(atPath: writePath) == false {
            FileManager.default.createFile(atPath: writePath, contents: nil, attributes: nil)
        }
    }
    
    
    func pause() -> Void {
        guard isDownload == true else { return }
        
        connection.cancel()
        
        appendFileData()
        
        syncInfoDic()
        
        isDownload = false
    }
    
    
    
    // MARK: - Private Methods
    
    /// 将接收到的缓存数据写入文件
    fileprivate func appendFileData() -> Void {
        guard receiveData.count > 0 else { return }
        
        let fileHandle = FileHandle.init(forWritingAtPath: writePath)
        
        fileHandle?.seekToEndOfFile()
        fileHandle?.write(receiveData)
        fileHandle?.closeFile()
        
        receiveData = Data.init()
    }
    
    
    fileprivate func getInfoDic() -> [String: Any] {
        
        if var userDef = UserDefaults.standard.object(forKey: DOWNLOADER_INFODIC) as? [String: Any] {
            if let infoDic = userDef[downloadURL.lastPathComponent] as? [String: Any] {
                return infoDic
            } else {
                let infoDic: [String: Any] = [:]
                userDef[downloadURL.lastPathComponent] = infoDic
                
                UserDefaults.standard.set(userDef, forKey: DOWNLOADER_INFODIC)
                UserDefaults.standard.synchronize()
                
                return infoDic
            }
        } else {
            let infoDic: [String: Any] = [:]
            let userDef = [downloadURL.lastPathComponent: infoDic]
            
            UserDefaults.standard.set(userDef, forKey: DOWNLOADER_INFODIC)
            UserDefaults.standard.synchronize()
            
            return infoDic
        }
        
    }
    
    
    fileprivate func syncInfoDic() -> Void {
        var userDef = UserDefaults.standard.object(forKey: DOWNLOADER_INFODIC) as? [String: Any]
        var infoDic = userDef?[downloadURL.lastPathComponent] as? [String: Any]
        
        infoDic?[DOWNLOAD_KEY_RECEIVELENGTH] = receiveLength
        infoDic?[DOWNLOAD_KEY_TOTALLENGTH] = totalLength
        
        userDef?[downloadURL.lastPathComponent] = infoDic
        UserDefaults.standard.set(userDef, forKey: DOWNLOADER_INFODIC)
        UserDefaults.standard.synchronize()
    }
    
    fileprivate func deleteFinishedInfo() -> Void {
        var userDef = UserDefaults.standard.object(forKey: DOWNLOADER_INFODIC) as? [String: Any]
        userDef?[downloadURL.lastPathComponent] = nil
        
        UserDefaults.standard.set(userDef, forKey: DOWNLOADER_INFODIC)
        UserDefaults.standard.synchronize()
    }
    
    
}



// MARK: - NSURLConnectionDataDelegate
extension Downloader: NSURLConnectionDataDelegate {
    
    func connection(_ connection: NSURLConnection, didReceive response: URLResponse) {
        if let httpResponse = response as? HTTPURLResponse {
            if totalLength == 0 {
                if let length = httpResponse.allHeaderFields["Content-Length"] as? NSString {
                    totalLength = length.integerValue
                }
            }
        }
    }
    
    
    func connection(_ connection: NSURLConnection, didReceive data: Data) {
        receiveData.append(data)
        receiveLength += data.count
        
        if receiveData.count > DOWNLOAD_MAXCACHE {
            appendFileData()
        }
        
        delegate?.downloader?(progress: progress, url: downloadURL)
    }
    
    
    func connectionDidFinishLoading(_ connection: NSURLConnection) {
        
        if receiveData.count < DOWNLOAD_MAXCACHE {
            appendFileData()
        }
        
        isDownload = false
        
        deleteFinishedInfo()

        delegate?.downloader?(didFinished: true, url: downloadURL, filePath: writePath)
    }
    
    
    
    
    
    
}

















