//
//  SecondViewController.swift
//  ExtensionProj-Swift
//
//  Created by peizhuo on 2016/12/9.
//  Copyright © 2016年 peizhuo. All rights reserved.
//

import UIKit

public enum PZTableViewCellEditingStyle {
    
    case none
    
    case delete
    
    case insert
    
    case move
}

class SecondViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var dataAry: [String] = []
    var cellStyle: PZTableViewCellEditingStyle = .delete
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for index in 1...40 {
            dataAry.append("row: \(index)")
        }

        
        let data = Data.init().subdata(in: Range(uncheckedBounds: (1,2)))
        print_debug(data)
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func editButtonAction(_ sender: UIBarButtonItem) {
        
        cellStyle = .move
        tableView.isEditing = !tableView.isEditing
        
    }
    
    
    @IBAction func addButtonAction(_ sender: UIBarButtonItem) {
        
        cellStyle = .insert
        tableView.isEditing = !tableView.isEditing
        
    }
    
    func deleteAction(sender: UISwipeGestureRecognizer) {
        cellStyle = .delete
        tableView.isEditing = !tableView.isEditing
    }
    

}


extension SecondViewController: UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataAry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        
//        cell?.allowRipple = true
        cell!.textLabel?.text = dataAry[indexPath.row]
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "删除"
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        let deleteAction = UITableViewRowAction.init(style: .default, title: "删除") { (action, indexP) in
//            
//        }
//        deleteAction.backgroundColor = UIColor.flatRed()
//        
//        return [deleteAction]
//    }
    
    
    // cell是否可以移动
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return cellStyle == .move
    }
    
    // 移动cell
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let moveItem = dataAry[sourceIndexPath.row]
        
        dataAry.remove(at: sourceIndexPath.row)
        
        dataAry.insert(moveItem, at: destinationIndexPath.row)
        
    }
    
    // 编辑cell
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print_debug(editingStyle.rawValue)
        
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.editingStyle == .delete {                   // 删除cell
            dataAry.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if cell?.editingStyle == .insert {            // 插入cell
            let newItem = "在row: \(indexPath.row)插入元素"
            dataAry.insert(newItem, at: indexPath.row)
            tableView.reloadData()
        }
    }
    
}


extension SecondViewController: UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        switch cellStyle {
        case .delete:
            return .delete
        case .insert:
            return .insert
        case .move:
            return .none
        default:
            return .none
        }
    }
    
    
    
    
}






















